const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("AsikToken", function () {
  it("Should return the new greeting once it's changed", async function () {
    const accounts = await ethers.getSigners();
    const deployer = accounts[0];
    const user = accounts[1];
    const Asik = await ethers.getContractFactory("AsikToken");
    const asik = await Asik.deploy(1000000);
    await asik.deployed();

    console.log('deployer', await asik.balanceOf(deployer.address));
    expect(await asik.balanceOf(deployer.address)).to.equal(1000000);
    await asik.transfer(user.address, 5000);

    console.log('user', await asik.balanceOf(user.address));
    console.log('deployer', await asik.balanceOf(deployer.address));
  });
});
